# Set up a container (using dry-container here)
class MyContainer
    extend Dry::Container::Mixin
  
    register "authentication_service" do
        AuthenticationService.new
    end
  
    register "user_service" do
        UserService.new
    end

    register "article_service" do
        ArticleService.new
    end
end
  
# Set up your auto-injection mixin
Import = Dry::AutoInject(MyContainer)