Rails.application.routes.draw do
  resources :articles,  only: [:create, :show, :update, :index, :destroy]
  post '/register',     to: 'users#create'
  post '/login',        to: 'authentication#login'

  resources :apidocs,  only: [:index]
  get '/api',       :to => redirect('/swagger-ui-dist/index.html')
end
