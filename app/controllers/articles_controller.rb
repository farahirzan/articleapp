class ArticlesController < ApplicationController
    before_action :authorize_request
    include Import["article_service"]

    def index
        data, error = article_service.index
        if error.nil?
            serialized_articles = ActiveModel::Serializer::CollectionSerializer.new(data, each_serializer: ArticleSerializer)
            render json: {status: 'SUCCESS', data: serialized_articles}, status: :ok 
        else
            render json: {status: 'ERROR', data: error}, status: :bad_request 
        end
    end

    def show
        data, error = article_service.show(params[:id])
        if error.nil?
            serialized_articles = ArticleSerializer.new(data)
            render json: {status: 'SUCCESS', data: serialized_articles}, status: :ok 
        else
            render json: {status: 'ERROR', data: error}, status: :bad_request 
        end
    end

    def create
        data, error = article_service.create(article_params)
        if error.nil?
            serialized_articles = ArticleSerializer.new(data)
            render json: {status: 'SUCCESS', data: serialized_articles}, status: :ok 
        else
            render json: {status: 'ERROR', data: error}, status: :bad_request 
        end
    end

    def destroy
        data, error = article_service.destroy(params[:id])
        if error.nil?
            serialized_articles = ArticleSerializer.new(data)
            render json: {status: 'SUCCESS', data: serialized_articles}, status: :ok 
        else
            render json: {status: 'ERROR', data: error}, status: :bad_request 
        end
    end

    def update
        data, error = article_service.update(article_params)
        if error.nil?
            serialized_articles = ArticleSerializer.new(data)
            render json: {status: 'SUCCESS', data: serialized_articles}, status: :ok 
        else
            render json: {status: 'ERROR', data: error}, status: :bad_request 
        end
    end
     
     private 
        def article_params
            params.permit(:id, :title, :body)
        end
end
