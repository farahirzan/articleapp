class AuthenticationController < ApplicationController
    include Import["authentication_service"]
    def login
        data, error = authentication_service.login(login_params)
        if error.nil?
            render json: {status: 'SUCCESS', data: data}, status: :ok 
        else
            render json: {status: 'ERROR', data: error}, status: :bad_request 
        end
    end

    private
    
        def login_params
            params.permit(:email, :password)
        end
end
