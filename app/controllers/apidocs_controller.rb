class ApidocsController < ApplicationController
    include Swagger::Blocks
    swagger_root do
        key :swagger, '2.0'
        info do
            key :version, '1.0.0'
            key :title, 'Article API'
            key :description, 'This is a API documentation for articles app'
            license do
                key :name, 'MIT'
            end
        end
        
        tag do
            key :name, 'articles'
            key :description, 'Articles operations'
            externalDocs do
                key :description, 'Find more info here'
                key :url, 'https://swagger.io'
            end
        end
        tag do
            key :name, 'user'
            key :description, 'Users operations'
            externalDocs do
                key :description, 'Find more info here'
                key :url, 'https://swagger.io'
            end
        end
        key :schemes, ['http']
        key :host, 'localhost:3000'
        key :basePath, '/'
        key :consumes, ['application/json']
        key :produces, ['application/json']
        security_definition :Bearer do
            key :type, :apiKey
            key :name, :Authorization
            key :in, :header
        end
    end

    # A list of all classes that have swagger_* declarations.
    SWAGGERED_CLASSES = [
        UsersSwaggerController,
        UserSwaggerSchema,
        ArticlesSwaggerController,
        ArticleSwaggerSchema,
        self,
    ].freeze

    def index
        render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
    end
end
