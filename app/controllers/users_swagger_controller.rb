class UsersSwaggerController < ActionController::Base
    include Swagger::Blocks
    swagger_path '/register' do
        operation :post do
            key :tags, ['user']
            key :summary, 'Register a new user'
            key :description, 'Creates a new user. Duplicates email are not allowed'
            key :operationId, 'register'
            key :consumes, [
                'application/json'
            ]
            key :produces, [
                'application/json'
            ]
            parameter do
                key :name, :body
                key :in, :body
                key :description, ''
                key :required, true
                schema do
                    key :'$ref', :User
                end
            end
            response 200 do
                key :description, 'Login successful'
                schema do
                  key :'$ref', :User
                end
            end
            response 400 do
                key :description, 'Login failed'
                schema do
                  key :'$ref', :User
                end
            end
        end
    end
    swagger_path '/login' do
        operation :post do
            key :summary, 'Login user'
            key :tags, ['user']
            key :operationId, 'login'
            key :consumes, [
                'application/json'
            ]
            key :produces, [
                'application/json'
            ]
            parameter do
                key :name, :body
                key :in, :body
                key :description, ''
                key :required, true
                schema do
                    key :'$ref', :User
                end
            end
            response 200 do
                key :description, 'Register successful'
                schema do
                  key :'$ref', :User
                end
            end
            response 400 do
                key :description, 'Register failed'
                schema do
                  key :'$ref', :User
                end
            end
        end
    end
end