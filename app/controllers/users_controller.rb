class UsersController < ApplicationController
  include Import["user_service"]
  def create
    data, error = user_service.create(user_params)
    if error.nil?
      render json: {status: 'SUCCESS', data: UserSerializer.new(data)}, status: :ok 
    else
      render json: {status: 'ERROR', data: error}, status: :bad_request 
    end
  end

  private
    def user_params
      params.permit(:email, :password, :password_confirmation)
    end
end
