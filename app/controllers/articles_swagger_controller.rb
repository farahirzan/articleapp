class ArticlesSwaggerController < ApplicationController
    include Swagger::Blocks
    swagger_path '/articles' do
        operation :get do
            key :tags, ['articles']
            key :summary, 'Get a list of articles'
            key :description, 'This endpoints uses a get request to retrieve all articles'
            key :operationId, 'getArticles'
            key :produces, [
                'application/json'
            ]
            response 200 do
                key :description, 'Operation successful'
                schema do
                  key :'$ref', :Article
                end
            end
            response 401 do
                key :description, 'Authorization required'
            end
            security do
                key :Bearer, []
            end
        end
        operation :post do
            key :tags, ['articles']
            key :summary, 'Add a new article'
            key :operationId, 'addArticle'
            key :consumes, [
                'application/json'
            ]
            key :produces, [
                'application/json'
            ]
            parameter do
                key :name, :body
                key :in, :body
                key :description, ''
                key :required, true
                schema do
                    key :'$ref', :ArticleInput
                end
            end
            response 200 do
                key :description, 'Operation successful'
                schema do
                  key :'$ref', :User
                end
            end
            response 400 do
                key :description, 'Failed to add new article'
                schema do
                  key :'$ref', :User
                end
            end
            response 401 do
                key :description, 'Authorization required'
            end
            security do
                key :Bearer, []
            end
        end
    end
    swagger_path '/articles/{id}' do
        operation :get do
            key :tags, ['articles']
            key :summary, 'Find article by ID'
            key :description, 'Returns a single article'
            key :operationId, 'getArticleById'
            key :produces, [
                'application/json'
            ]
            parameter do
                key :name, :id
                key :in, :path
                key :description, 'ID of article'
                key :required, true
            end
            response 200 do
                key :description, 'successful operation'
                schema do
                  key :'$ref', :Article
                end
            end
            response 401 do
                key :description, 'Authorization required'
            end
            security do
                key :Bearer, []
            end
        end
        operation :put do
            key :tags, ['articles']
            key :summary, 'Edit an article'
            key :operationId, 'editArticle'
            key :consumes, [
                'application/json'
            ]
            key :produces, [
                'application/json'
            ]
            parameter do
                key :name, :id
                key :in, :path
                key :description, 'ID of article'
                key :required, true
            end
            parameter do
                key :name, :body
                key :in, :body
                key :description, ''
                key :required, true
                schema do
                    key :'$ref', :ArticleInput
                end
            end
            response 200 do
                key :description, 'successful operation'
                schema do
                  key :'$ref', :User
                end
            end
            response 400 do
                key :description, 'failed to update article'
                schema do
                  key :'$ref', :User
                end
            end
            response 401 do
                key :description, 'Authorization required'
            end
            security do
                key :Bearer, []
            end
        end
        operation :delete do
            key :tags, ['articles']
            key :summary, 'Find article by ID'
            key :description, 'Returns a single article'
            key :operationId, 'getArticleById'
            key :produces, [
                'application/json'
            ]
            parameter do
                key :name, :id
                key :in, :path
                key :description, 'ID of article'
                key :required, true
            end
            response 200 do
                key :description, 'successful operation'
                schema do
                  key :'$ref', :Article
                end
            end
            response 401 do
                key :description, 'Authorization required'
            end
            security do
                key :Bearer, []
            end
        end
    end
end
