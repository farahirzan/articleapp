class ArticleService
    def index
        begin
            articles = Article.order('created_at DESC');
            return articles, nil
        rescue
            return nil, "Failed to load article"
        end
    end

    def show(id)
        begin
            article = Article.find(id)
            return article, nil
        rescue
            return nil, "Failed to load article"
        end
    end
 
    def create(article_params)
        article = Article.new(article_params)

        if article.save
            return article, nil
        else
            return nil, "Failed to create article"
        end
    end

    def destroy(id)
        begin
            @article = Article.find(id)
        rescue
            return nil, "Article not found"
        end

        if @article.destroy
            return @article, nil
        else
            return nil, "Failed to delete article"
        end
    end

    def update(article_params)
        begin
            article = Article.find(article_params[:id])
        rescue
            return nil, "Article not found"
        end

        if article.update_attributes(article_params)
            return article, nil
        else
            return nil, "Failed to update article"
        end
    end
end