class UserService
    def create(user_params)
        @user = User.new(user_params)
        if @user.save
            return @user, nil
        else
            return nil, @user.errors
        end
      end
end