class AuthenticationService
    def login(login_params)
        begin
            @user = User.find_by(email:login_params[:email])
        rescue
            return nil, "User not found"
        end
        
        if @user&.authenticate(login_params[:password])
            token = JsonWebToken.encode(user_id: @user.id)
            return token, nil
        else
            return nil, "Login failed"
        end
    end
end