class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  field :email, type: String
  field :password_digest, type: String

  before_save { self.email = email.downcase }
  validates :email, presence: true, 
                    length: { maximum: 255 },
                    uniqueness: true

  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
end