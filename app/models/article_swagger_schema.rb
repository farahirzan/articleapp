class ArticleSwaggerSchema
    include Swagger::Blocks
    swagger_schema :Article do
      key :required, [:id]
      property :id do
        key :type, :string
      end
      property :title do
        key :type, :string
      end
      property :body do
        key :type, :string
      end
    end

    swagger_schema :ArticleInput do
      property :title do
        key :type, :string
      end
      property :body do
        key :type, :string
      end
    end
end