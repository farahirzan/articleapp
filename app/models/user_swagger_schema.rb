class UserSwaggerSchema
    include Swagger::Blocks
  
    swagger_schema :User do
      key :required, [:email, :password]
      property :email do
        key :type, :string
      end
      property :password do
        key :type, :string
      end
    end
end