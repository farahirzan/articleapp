5.times do
    article = Article.new(
        title: Faker::Superhero.name,
        body: Faker::Lorem.paragraphs
    )

    article.save
end